<?php get_header(); ?>


<section class="section-padding" id="">
	<div class="grid-container">
		<div class="grid-x">

			<div class="cell">
				<h1>Resultado de busca</h1>
			</div>

			<div class="cell">
				<p>Termo buscado: <?= $_GET['s'] ?></p>
			</div>

			<div class="cell">
				<?php if ( have_posts() ) : ?>
					<ul class="menu vertical">
						<?php while ( have_posts() ) : the_post(); ?>
							<?php// var_dump($post); ?>
							<li>
								<a href="<?php the_permalink(); ?>">
									<?php the_title(); ?> <small>(<?=$post->post_type ?>)</small>
								</a>
							</li>
						<?php endwhile?>
					</ul>
				<?php else: ?>
					<p>Nenhum resultado para o termo solicitado.</p>
				<?php endif; ?>
			</div>

		</div>
	</div>
</section>

<?php get_footer(); ?>
