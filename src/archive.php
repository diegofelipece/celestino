<?php get_header();

$requested = $_SERVER['REQUEST_URI'];
$archive = explode('/', $requested);
$archive_y = $archive[1];
$archive_m = $archive[2];
?>

<section class="section-padding" id="">
	<div class="grid-container">
		<div class="grid-x">

				<div class="cell medium-10 medium-offset-1 text-center is-uppercase">
					<h1>
						<?="{$archive_m}/{$archive_y}" ?>
					</h1>
				</div>

		</div>
	</div>
</section>

<?php
get_template_part('fragments/posts_item');
get_footer();
?>
