import $ from 'jquery';
import whatInput from 'what-input';
import Foundation from './lib/foundation-explicit-pieces';
window.$ = $;
window.tangerine = tangerine;

// import owlCarousel from 'owl.carousel';
require('./lib/owl.js');
require('./lib/parallax.js');

// require('./lib/custom.js');
require('./lib/autocomplete.js');
require('./lib/maps.js');

$(document).foundation();

//Wow
let wowJs = document.createElement('script');
wowJs.src = `${tangerine.template_url}/vendors/wow.min.js`;

document.body.appendChild(wowJs);
wowJs.onload = function(){
  let wow = new WOW({
    boxClass: 'wow',
    animateClass: 'is-animating'
  }).init();
};

// Navigate on magellan via url
if (window.location.hash) {
  $('[data-magellan]').foundation('scrollToLoc', window.location.hash);

  if ($('[data-accordion]')) {
    $(`.accordion ${window.location.hash}`).trigger('click');
  }
}

// Modal
function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires="+d.toUTCString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  let name = cname + "=";
  let ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
      }
  }
  return "";
}

let modal = $('#modal-welcome');
if (modal.length) {

  let mShowed = getCookie('modal_showed');
  if (mShowed != "") {

  } else {
    modal.foundation('open');

    modal.on('closed.zf.reveal', (event) => {
      setCookie('modal_showed', 'true', 30);
    });
  }

}
