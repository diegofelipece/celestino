import $ from 'jquery';
window.jQuery = $;
const owlCarousel = require('owl.carousel');

const owlCarouselElm = $('[data-owlCarousel]');
if (owlCarouselElm.length) {
  owlCarouselElm.each(function(index, el) {
    let data = jQuery.parseJSON($(this).attr('data-owlCarousel'));
    // console.log(data);
    // console.log($(this));
    $(this).owlCarousel(data);
  });
}
