import $ from 'jquery';
window.jQuery = $;

function initMap(target, centerCord) {
    let options = {
        zoom: 6,
        center: centerCord,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        scrollwheel: false,
        styles: [
          {"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[
            {"visibility":"on"},
            {"color":"#e0efef"}]
          },
          {"featureType":"poi.business","elementType":"all","stylers":[
            {"visibility":"off"}]
          },
          {"featureType":"poi.medical","elementType":"all","stylers":[
            {"visibility":"off"}]
          },
          {"featureType":"poi","elementType":"geometry.fill","stylers":[
            {"visibility":"on"},
            {"hue":"#1900ff"},
            {"color":"#c0e8e8"}]
          },
          {"featureType":"road","elementType":"geometry","stylers":[
            {"lightness":100},
            {"visibility":"simplified"}]
          },
          {"featureType":"road","elementType":"labels","stylers":[
            {"visibility":"off"}]
          },
          {"featureType":"transit.line","elementType":"geometry","stylers":[
            {"visibility":"on"},
            {"lightness":700}]
          },
          {"featureType":"water","elementType":"all","stylers":[
            {"color":"#7dcdcd"}]
          }]
    };

    return new google.maps.Map(document.getElementById('map'), options);
}

var markers = [];
function hideAllInfoWindows(map) {
  markers.forEach(function(marker) {
    marker.infowindow.close(map, marker);
  });
}

function addMakers(map, pins) {
   let latlngbounds = new google.maps.LatLngBounds();
   let infoWindowActive;

   pins.map((pin) => {
     let thisPin = pin.map;

     let infowindow = new google.maps.InfoWindow({
       content: `<div class="info-window-wrapper">
                  <h3 class="inner_title">${pin.title}</h3>
                  <p class="inner_content">${pin.info}</p>
                </div>`
     });

     let marker = new google.maps.Marker({
         position: new google.maps.LatLng(thisPin.lat, thisPin.lng),
         title: pin.title,
         map: map,
         icon: `${window.tangerine.template_url}/img/pin.svg`,
         infowindow: infowindow
     });

     markers.push(marker);

     marker.addListener('click', function () {
       hideAllInfoWindows(map);
       infowindow.open(map, marker);
     });

     latlngbounds.extend(marker.position);
   });

   map.fitBounds(latlngbounds);
}

const mapWrapper = $('[data-map]');

if (mapWrapper.length) {
  const partners = JSON.parse(mapWrapper.attr('data-map'));

  let gMapsAPI = document.createElement('script');
  gMapsAPI.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBvw1qzksfq-I9BysBSVSqUDoAmBZQqJhE';

  document.body.appendChild(gMapsAPI);
  gMapsAPI.onload = function(){
    let firstPin = partners[0].map;
    firstPin.lat = Number(firstPin.lat);
    firstPin.lng = Number(firstPin.lng);

    let map = initMap(mapWrapper, firstPin);
    addMakers(map, partners);
  };

};
