<?php get_header();

$this_page = get_queried_object();
$thumb_id = get_post_thumbnail_id( $this_page->ID );
?>

<?php if ($thumb_id) : ?>
	<div class="page-header" data-interchange="[<?php getImage($thumb_id, 'page-header') ?>, small]"></div>
<?php endif; ?>

<section class="section-padding" id="">
	<div class="grid-container">
		<div class="grid-x">

				<div class="cell medium-10 medium-offset-1 text-center is-uppercase">
					<h1><?=$this_page->post_title ?></h1>
				</div>

		</div>
	</div>
</section>

<?php
get_template_part('fragments/posts_item');
get_footer();
?>
