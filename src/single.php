<?php get_header();

$thumb_id = get_post_thumbnail_id( $post->ID );
?>

<?php if ($thumb_id) : ?>
	<div class="page-header" data-interchange="[<?php getImage($thumb_id, 'page-header') ?>, small]"></div>
<?php endif; ?>

<section class="section-padding">

	<div class="grid-container">
		<div class="grid-x grid-margin-x">

			<div class="cell medium-8">
				<div class="grid-x">

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
						<div class="cell">
							<h1><?php the_title(); ?></h1>
						</div>
						<div class="cell">
							<?php
							$categories = get_categories();
							if ($categories) :
								?>
								<ul class="menu">
									<?php foreach ($categories as $category) : ?>
										<li><a href="<?=home_url().'/'.$category->slug?>" class="button secondary"><?=$category->name?></a></li>
									<?php endforeach; ?>
								</ul>
								<br>
							<?php endif; ?>
						</div>
						<div class="cell">
							<?php the_content(); ?>
						</div>
					<?php endwhile?>
				<?php else: ?>
				<?php endif; ?>

				</div>
			</div>

			<?php if ( is_active_sidebar('blog-sidebar') ) : ?>
				<div class="cell medium-4 blog-sidebar animated wow fadeInUp" data-wow-delay="0.2s">
					<ul>						
						<?php dynamic_sidebar('blog-sidebar'); ?>
					</ul>
				</div>
			<?php endif; ?>


		</div>
	</div>

</section>

<?php get_footer(); ?>
