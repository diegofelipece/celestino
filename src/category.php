<?php get_header();

$this_page = get_queried_object();
?>

<section class="section-padding" id="">
	<div class="grid-container">
		<div class="grid-x">

				<div class="cell medium-10 medium-offset-1 text-center is-uppercase">
					<h1>
						<?=$this_page->name ?>
					</h1>
				</div>

		</div>
	</div>
</section>

<?php
get_template_part('fragments/posts_item');
get_footer();
?>
