<?php get_header(); ?>

<section class="section-padding" id="">
	<div class="grid-container">
		<div class="grid-x text-center">

			<div class="cell medium-10 medium-offset-1 is-uppercase">
				<h1>Erro 404</h1>
			</div>

			<div class="cell medium-8 medium-offset-2">
				<p>A página solicitada não foi encontrada.</p>
			</div>

		</div>
	</div>
</section>

<?php get_footer(); ?>
