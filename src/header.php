<!doctype html>
<html lang="<?php bloginfo('language') ?>">
<head>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
	<meta property="og:locale" content="<?php bloginfo('language') ?>">

	<?php
		$tags_header = get_field('tags_header', 'option');
		echo ($tags_header) ? $tags_header : '';
	?>

	<meta name="description" content="<?php bloginfo('description')?>">
	<title><?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?></title>

	<style media="screen">
		.wow { visibility: hidden; };
	</style>

	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_url'); ?>/css/app.css?v=<?=getThemeVersion()?>">

	<?php get_template_part('fragments/favicons'); ?>

	<?php wp_head() ?>
</head>

<body <?php body_class()?>>

<?php
	$tags_body = get_field('tags_body', 'option');
	echo ($tags_body) ? $tags_body : '';
?>

<header data-sticky-container>
	<div class="header-menu" data-sticky data-options="marginTop:0;" style="width:100%">
		<div class="grid-container">
			<div class="grid-x">

				<div class="cell">

					<div class="title-bar wow animate fadeIn" data-wow-duration="1s" data-responsive-toggle="responsive-menu" data-hide-for="large">
					  <button class="menu-icon" type="button" data-toggle="responsive-menu"></button>
						<button type="button" data-toggle="responsive-menu">
							<img src="<?php bloginfo('template_url') ?>/img/celestino-h.svg" width="222" alt="<?php bloginfo('name') ?>">
						</button>
					</div>

					<div class="top-bar" id="responsive-menu">
						<div class="top-bar-left show-for-large">
							<a href="<?=home_url()?>"><img src="<?php bloginfo('template_url') ?>/img/celestino-h.svg" width="222" alt="<?php bloginfo('name') ?>"></a>
						</div>
						<div class="top-bar-right">
							<?php
							wp_nav_menu( array(
								'theme_location' => 'primary',
								'menu_class' => 'menu medium-horizontal vertical text-center',
								'container' => ''
							));
							wp_nav_menu( array(
								'theme_location' => 'contact',
								'menu_class' => 'menu complement-line align-right',
								'container' => ''
							));
							?>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
</header>

<div class="full-bar">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<?php
		wp_nav_menu( array(
			'theme_location' => 'submenu',
			'menu_class' => 'menu align-right',
			'container' => ''
		));
	 ?>
	</div>
</div>

<div class="sections-wrapper">
