<?php
add_action( 'init', 'createPostTypes');

function createPostTypes(){
  register_post_type('destino',
    array(
      'labels' => array(
        'name' => __( 'Destinos'),
        'singular_name' => __( 'Destino')
      ),
      // 'taxonomies' => array('produtos'),
      'public' => true,
      'show_ui' => true,
      'exclude_from_search' => false,
      'hierarchical' => true,
      'supports' => array( 'title', 'editor', 'thumbnail'),
      'query_var' => true,
      'has_archive' => true,
      'menu_position'=> 5,
      'menu_icon' => 'dashicons-admin-site',
      'show_in_rest'=> true,
      'rest_base'=> 'destino',
      'rest_controller_class' => 'WP_REST_Posts_Controller'
    )
  );

  register_post_type('representante',
    array(
      'labels' => array(
        'name' => 'Representantes',
        'singular_name' => 'Representante'
      ),
      'public' => true,
      'show_ui' => true,
      'exclude_from_search' => false,
      'hierarchical' => true,
      'supports' => array( 'title', 'editor', 'thumbnail'),
      'query_var' => true,
      'has_archive' => true,
      'menu_position'=> 5,
      'menu_icon' => 'dashicons-location-alt',
      'show_in_rest'=> true,
      'rest_base'=> 'destino',
      'rest_controller_class' => 'WP_REST_Posts_Controller'
    )
  );

}
