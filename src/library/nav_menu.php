<?php
add_action('after_setup_theme', 'register_menu' );
function register_menu() {
  register_nav_menu( 'primary', __( 'Menu Principal', 'celestino-theme' ) );
  register_nav_menu( 'contact', __( 'Contato', 'celestino-theme' ) );
  register_nav_menu( 'social', __( 'Redes Sociais', 'celestino-theme' ) );
  register_nav_menu( 'submenu', __( 'Submenu', 'celestino-theme' ) );
}
?>
