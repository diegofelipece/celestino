<?php

add_action('init', 'images_suport');
function images_suport(){
  if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );
  if ( function_exists( 'add_image_size' ) ) {
    add_image_size( 'page-header', 1440, 180, true );
    add_image_size( 'page-header@2x', 2880, 360, true );
    add_image_size( 'card', 300, 220, true );
    add_image_size( 'card@2x', 600, 440, true );
    add_image_size( 'carousel-large', 1440, 500, true );
    add_image_size( 'carousel-medium', 768, 500, true );
    add_image_size( 'open_graph', 800, 600, true );
  }
}
