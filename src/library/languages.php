<?php

add_action('after_setup_theme', 'my_theme_setup');
function my_theme_setup(){
    load_theme_textdomain('celestino-theme', get_template_directory() . '/languages');
}

// CHANGE LOCAL LANGUAGE
// must be called before load_theme_textdomain()
add_filter( 'locale', 'my_theme_localized' );
function my_theme_localized( $locale )
{
	if ( isset( $_GET['pt_BR'] ) )
	{
		return sanitize_key( $_GET['pt_BR'] );
	}

	return $locale;
}
