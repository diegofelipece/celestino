<?php
function getThemeVersion(){
  $theme = wp_get_theme();
  return $theme->get( 'Version' );
}
