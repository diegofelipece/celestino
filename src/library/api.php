<?php

function api_get_destinations(){

  $encode = false;
  $destinations = get_destinations_JSON($encode);

  return rest_ensure_response( $destinations );
}

function register_destinations_routes(){

  register_rest_route('widget/v1', '/destinations', array(
    'methods' => 'GET',
    'callback' => 'api_get_destinations',
  ));
}
add_action('init', 'register_destinations_routes');
