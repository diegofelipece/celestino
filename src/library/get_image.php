<?php

function getImage($id, $size){
	$image_src = wp_get_attachment_image_src( $id, $size, 0);
	echo $image_src[0];
};

function get_image($id, $size){
	$image_src = wp_get_attachment_image_src( $id, $size, 0);
	return $image_src[0];
};
