<?php
class Destination{

	public $id;
	public $slug;
	public $title;
	public $url;
	public $visa_info;
}

function get_destinations_JSON( $encode = true ){

	$destinatons_query = new WP_Query(array(
		'post_type' => 'destino',
		'posts_per_page'=> -1
	));
	$destinations = $destinatons_query->posts;
	$destinations_result = array();
	$baseURL = get_bloginfo('url');

  while ($destinatons_query->have_posts()) : $destinatons_query->the_post();
    $d = new Destination;
    $d->id = get_the_ID();
    $d->slug = get_post_field('post_name');
    $d->title = get_the_title();
    $d->url = get_the_permalink();
    $d->visa_info = array(
      'tourist' => (get_field('destination_tourist')) ? true : false,
      'study' => (get_field('destination_study')) ? true : false,
      'business' => (get_field('destination_business')) ? true : false
    );
    array_push($destinations_result, $d);
  endwhile;
  wp_reset_postdata();

  if ($encode) {
    return json_encode($destinations_result);
  }
  return $destinations_result;
}
