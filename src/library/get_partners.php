<?php
function get_partners_json(){

  $representantes = new wp_query(array( 'post_type' => 'representante' ));
  $partners = [];

  while ($representantes->have_posts()) : $representantes->the_post();
    $id = get_the_ID();
    $acf = get_fields($id);

    $info = "$acf[representante_address] <br> $acf[representante_city]-$acf[representante_state] <br> CEP: $acf[representante_cep] <br>";

    if( have_rows('representante_telefones') ):
      while ( have_rows('representante_telefones') ) : the_row();
        $phone = get_sub_field('representante_telefones_each');
        $phone_filtered = preg_filter('/\)*\(*\s*/', '', $phone);
        $info .= '<a href="tel:+55'.$phone_filtered.'" target="_blank">'.$phone.'</a><br>';
      endwhile;
    endif;

    if( have_rows('representante_emails') ):
      while ( have_rows('representante_emails') ) : the_row();
        $email = get_sub_field('representante_emails_each');
        $info .= '<a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br>';
      endwhile;
    endif;

    $site = get_field('representante_site', $id);
    if( $site ):
      $info .= '<a href="'.$site.'" target="_blank">'.$site.'</a><br>';
    endif;

    $partner = array(
      'title' => get_the_title(),
      'info' => $info,
      'map' => $acf[representante_map]
    );
    //	var_dump($partner);
    array_push($partners, $partner);
  endwhile;

  return json_encode($partners);
}
