<?php
function get_canonical(){
	if (is_front_page()) {

		return get_bloginfo('url');
	} elseif (is_category()) {

		return get_bloginfo('url').$_SERVER['REQUEST_URI'];
	} elseif (is_archive()) {

		return get_bloginfo('url').$_SERVER['REQUEST_URI'];
	} elseif (is_home()) {

		$post = get_queried_object();
		return get_bloginfo('url').'/'.$post->post_name;
	} elseif (is_tax()) {

		$term = get_queried_object();
		return get_bloginfo('url').'/'.$term->slug;
	};
	return wp_get_canonical_url();
}
