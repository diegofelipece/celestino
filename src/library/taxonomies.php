<?php

add_action( 'init', 'createTaxonomies');

function createTaxonomies() {
	register_taxonomy(
		'produtos',
   	'produto',
		array(
			'hierarchical' => true,
			'label' => __( 'Product Line' ),
			'query_var' => true,
			'rewrite' => true
		)
	);
}
