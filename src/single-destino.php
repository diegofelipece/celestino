<?php get_header();
$thumb_id = get_post_thumbnail_id( $post->ID );
?>

<?php if ($thumb_id) : ?>
	<div class="page-header" data-interchange="[<?php getImage($thumb_id, 'page-header') ?>, small]"></div>
<?php endif; ?>

<section class="section-padding destination-page">

	<div class="grid-container">
		<div class="grid-x">
			<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<div class="cell is-uppercase __title">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="cell" data-magellan data-offset="120">
					<div class="grid-x grid-margin-x __info">
						<div class="cell medium-2 __info-flag">
							<img src="<?php the_field('destination_flag')?>" alt="" width="140">
						</div>
						<div class="cell medium-10 __info-data">
							<ul class="menu vertical">
								<li>Capital: <?php the_field('destination_capital');?></li>
								<li>Estatuto: <?php the_field('destination_govern');?></li>
								<li>Idioma: <?php the_field('destination_language');?></li>
								<li>Moeda: <?php the_field('destination_currency');?></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="cell __info" data-magellan data-offset="120">
					<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">

						<?php if (get_field('destination_tourist')): ?>
							<li class="accordion-item" data-accordion-item id="turismo" data-magellan-target="turismo">
								<a href="#" class="accordion-title">Turismo</a>

								<div class="accordion-content" data-tab-content>
									<?php the_field('destination_tourist') ?>
								</div>
							</li>
						<?php endif; ?>

						<?php if (get_field('destination_business')): ?>
						  <li class="accordion-item" data-accordion-item id="negocios" data-magellan-target="negocios">
						    <a href="#" class="accordion-title">Negócios</a>

						    <div class="accordion-content" data-tab-content>
									<?php the_field('destination_business') ?>
						    </div>
						  </li>
						<?php endif; ?>

						<?php if (get_field('destination_study')): ?>
						  <li class="accordion-item" data-accordion-item id="estudos" data-magellan-target="estudos">
						    <a href="#" class="accordion-title">Estudos</a>

						    <div class="accordion-content" data-tab-content>
									<?php the_field('destination_study') ?>
						    </div>
						  </li>
						<?php endif; ?>

					</ul>
				</div>

				<?php if( have_rows('destination_files') ):?>
					<div class="cell is-uppercase __title">
						<h3>Formulários</h3>
					</div>
					<div class="cell __info">
						<ul class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">

							<?php while ( have_rows('destination_files') ) : the_row();?>
								<li class="accordion-item" data-accordion-item id="<?php the_sub_field('destination_files_id') ?>" data-magellan-target="<?php the_sub_field('destination_files_id') ?>">
									<a href="#" class="accordion-title"><?php the_sub_field('destination_files_title') ?></a>

									<div class="accordion-content" data-tab-content>
										<?php the_sub_field('destination_files_form') ?>
									</div>
								</li>
					    <?php endwhile;?>

						</ul>
					</div>


				<?php endif;?>


			<?php endwhile?>
		<?php else: ?>
		<?php endif; ?>
	</div>
</div>

</section>

<?php
get_template_part('fragments/search_box');

if (get_field('blog_show_posts_destinies', 'option')) {
	$post = query_posts( array(
		'taxonomy'  => 'category',
		'posts_per_page' => 3
	) );
	get_template_part('fragments/posts_item');
}
?>

<?php get_footer(); ?>
