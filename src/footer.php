<?php wp_footer(); ?>

<?php if (get_field('modal_show', 'option')): ?>
  <div class="reveal" id="modal-welcome" data-reveal>
    <?php the_field('modal_content', 'option') ?>
    <button class="close-button" data-close aria-label="Close modal" type="button">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
<?php endif; ?>

</div><!-- .sections-wrapper -->

<footer class="footer-bar">
  <div class="grid-container">
    <div class="grid-x grid-margin-x grid-margin-y">
      <address class="cell medium-8 __contact text-center medium-text-left">
        <a href="#">Av. São Luís, 50 24° Andar Conj. 242ABC Edifício Itália - Centro São Paulo, S.P.</a>
        <?php
        wp_nav_menu( array(
          'theme_location' => 'contact',
          'menu_class' => 'menu small-align-center',
          'container' => ''
        ));
        ?>
      </address>
      <div class="cell medium-4">
        <?php
        wp_nav_menu( array(
          'theme_location' => 'social',
          'menu_class' => 'menu menu-social align-right',
          'container' => ''
        ));
        ?>
      </div>
      <div class="cell __copyright text-center medium-text-left">
        © Celestino - Todos os direitos reservados - Criado por <a href="https://www.agenciabambu.com.br/" target="_blank">Bambu</a>
      </div>

    </div>
  </div>
</footer>

<script type="text/javascript">
  window.tangerine = {
    "home_url": "<?=home_url()?>",
    "template_url": "<?= bloginfo('template_url')?>"
  };
</script>
<script src="<?php bloginfo('template_url');?>/js/app.js?v=<?=getThemeVersion()?>"></script>

<?php
	$tags_footer = get_field('tags_footer', 'option');
	echo ($tags_footer) ? $tags_footer : '';
?>

</body>
</html>
