<?php

// Check for new version
require 'library/plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'http://cliente.diegoesteves.ink/themes/celestino/theme.json',
	__FILE__,
	'celestino-theme'
);

// maps
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyBvw1qzksfq-I9BysBSVSqUDoAmBZQqJhE');
}
add_action('acf/init', 'my_acf_init');

// Options Page
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
			'page_title' 	=> 'Ajustes do Tema',
			'menu_title'	=> 'Ajustes do Tema',
			'menu_slug' 	=> 'theme-general-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false
		));
}

/**
 * Register our sidebars and widgetized areas.
 *
 */
function contact_widgets_init() {

	register_sidebar( array(
		'name'          => 'Blog Sidebar',
		'id'            => 'blog-sidebar'
	) );

}
add_action( 'widgets_init', 'contact_widgets_init' );

// Destinations Model
require_once( 'library/destinations.php' );

// Partners
require_once( 'library/get_partners.php' );

// Widget API
require_once( 'library/api.php' );

// Get Theme Version
require_once( 'library/get_theme_version.php' );

// Get Theme Version
require_once( 'library/get_canonical.php' );

// Nav menu
require_once( 'library/nav_menu.php' );

// Post type and taxonomys
require_once( 'library/post_types.php' );

// Thumbs
require_once( 'library/thumbs.php' );

// Languages
// require_once( 'library/languages.php' );

// Get Images
require_once( 'library/get_image.php' );
