<?php
/* Template Name: Contact */
get_header();

$thumb_id = get_post_thumbnail_id( $post->ID );
?>

<?php if ($thumb_id) : ?>
	<div class="page-header" data-interchange="[<?php getImage($thumb_id, 'page-header') ?>, small]"></div>
<?php endif; ?>


	<section class="section-padding contact-page" id="">
		<div class="grid-container">
			<div class="grid-x">

				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<div class="cell medium-8 medium-offset-2 text-center is-uppercase">
						<h1><?php the_title(); ?></h1>
						<br>
					</div>

					<div class="cell medium-6 medium-offset-1">
						<?php the_content(); ?>
					</div>

				<?php endwhile?>
				<?php else: ?>
				<?php endif; ?>


				<?php if (get_field('contact_form')) : ?>
					<div class="cell medium-10 medium-offset-1">
						<h2 class="__subtitle is-uppercase">Mande uma mensagem</h2>
						<p>você também pode preencher o seguinte formulário:</p>
					</div>
					<div class="cell medium-10 medium-offset-1 __form">
						<?= do_shortcode( get_field('contact_form') ); ?>
					</div>
				<?php endif; ?>

				<?php if (get_field('contact_form')) : ?>
					<div class="cell medium-4 medium-offset-4">
						<hr>
					</div>
	        <div class="cell medium-10 medium-offset-1">
						<?php the_field('contact_map'); ?>
	        </div>
				<?php endif; ?>

			</div>
		</div>
	</section>


<?php get_footer(); ?>
