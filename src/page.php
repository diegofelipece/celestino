<?php get_header();

$thumb_id = get_post_thumbnail_id( $post->ID );
?>

<?php if ($thumb_id) : ?>
	<div class="page-header" data-interchange="[<?php getImage($thumb_id, 'page-header') ?>, small]"></div>
<?php endif; ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<?php if (get_field('show_title')) : ?>
		<section class="section-padding" id="">
			<div class="grid-container">
				<div class="grid-x">

						<div class="cell medium-10 medium-offset-1 text-center is-uppercase">
							<h1><?php the_title(); ?></h1>
						</div>

					<div class="cell medium-8 medium-offset-2">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
	<?php endif; ?>

<?php endwhile?>
<?php else: ?>
<?php endif; ?>

<?php get_template_part('fragments/global_components'); ?>

<?php get_footer(); ?>
