<?php
$loop = get_sub_field('carousel_config_loop');
$autoplay = get_sub_field('carousel_config_autoplay');
$autoplaySpeed = get_sub_field('carousel_config_autoplaySpeed');
$autoplayTimeout = get_sub_field('carousel_config_autoplayTimeout');
$id = get_sub_field('carousel_id');
?>

<section class="carousel" id="<?= ($id) ? $id : '' ?>">
	<div class="owl-carousel carousel-wrap grid-x wow animate fadeIn" data-wow-duration="1s"
	data-owlCarousel='{
		"items": 1,
		"loop": <?= ($loop) ? 'true' : 'false' ?>,
		"autoplay": <?= ($autoplay) ? 'true' : 'false' ?>,
		"autoplaySpeed": <?= ($autoplaySpeed) ? $autoplaySpeed : '800' ?>,
		"autoplayTimeout": <?= ($autoplayTimeout) ? $autoplayTimeout : '6000' ?>
	}'>
	<?php if( have_rows('carousel_item') ):?>

		   <?php
			 	while ( have_rows('carousel_item')) : the_row();
				$url = get_sub_field('carousel_item_url');
			?>
						<div class="cell carousel-each" data-interchange="
							[<?php getImage(get_sub_field('carousel_item_img_small'), 'carousel') ?>, small],
							[<?php getImage(get_sub_field('carousel_item_img'), 'carousel') ?>, medium]
						">

							<?php if ($url) : ?>
								<a href="<?=$url?>" class="__clicktag" target="<?php the_sub_field('carousel_item_target'); ?>"></a>
							<?php endif; ?>

							<div class="grid-container">
								<div class="grid-x">
									<div class="cell __caption">
										<?php the_sub_field('carousel_item_text'); ?>
									</div>
								</div>
							</div>
						</div>

			<?php endwhile; ?>

	<?php else :?>


	<?php endif;?>

	</div>
</section>
