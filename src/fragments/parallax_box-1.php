<?php $url = get_sub_field('parallax_box-1_main-url'); ?>
<section class="parallax-box" data-parallax="scroll" data-image-src="<?php the_sub_field('parallax_box-1_bg'); ?>">
		<div class="inner-full-bar text-center wow animate fadeIn" data-wow-duration="1s">
			<a href="<?php the_sub_field('parallax_box-1_upper-url'); ?>"><?php the_sub_field('parallax_box-1_upper-text'); ?></a>
		</div>

		<div class="grid-container centered-container wow animate fadeIn" data-wow-duration="1s">
			<div class="grid-y">
				<div class="cell medium-6 medium-offset-6 large-4 large-offset-8 centered-container-inner has-alert text-right is-uppercase">
					<?= ($url) ? '<a href="'.$url.'" class="__clicktag"></a>' : ''?>
					<?php the_sub_field('parallax_box-1_main-text'); ?>
				</div>
			</div>
		</div>

</section>
