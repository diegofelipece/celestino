<section class="section-padding text-center round-list" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x grid-margin-y">

			<div class="cell __title">
				<h2><?php the_sub_field('round_list_title'); ?></h2>
			</div>

			<div class="cell large-10 large-offset-1 __itens">
				<div class="grid-x grid-margin-x grid-margin-y">

					<?php if( have_rows('round_list_item') ):?>
						<?php while ( have_rows('round_list_item')) : the_row(); ?>

						<div class="cell medium-6 large-3 __itens-each">
							<a href="<?php the_sub_field('round_list_item_url') ?>" class="__itens-each-thumb" data-interchange="[<?php the_sub_field('round_list_item_image') ?>, small]">
								<div class="__itens-each-thumb-label"><?php the_sub_field('round_list_item_text') ?></div>
							</a>
						</div>
					<?php endwhile; ?>
					<?php else :?>
					<?php endif;?>

				</div>
			</div>

			<div class="cell __subtitle">
				<?php the_sub_field('round_list_text'); ?>
			</div>

			<div class="cell">
				<a href="<?php the_sub_field('round_list_url'); ?>" class="button round secondary"><i class="icon-up-open-big"></i></a>
			</div>

		</div>
	</div>
</section>
