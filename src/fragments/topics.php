<section class="topics has-border-top section-padding">

  <div class="grid-container wow animate fadeIn" data-wow-duration="1s">
    <div class="grid-x grid-margin-y text-center">

		<?php if( have_rows('topics_item') ):?>
        <div class="cell large-1"></div>

				<?php while ( have_rows('topics_item')) : the_row(); ?>
          <div class="cell medium-4 large-offset-1 large-2 topics-each">
            <div class="grid-x">
              <div class="cell __title">
                <?php the_sub_field('topics_item_title') ?>
              </div>
              <div class="cell">
                <div class="button"><i class="icon-down-open-big"></i></div>
              </div>
              <div class="cell __content">
                <?php the_sub_field('topics_item_text') ?>
              </div>
            </div>
          </div>
				<?php endwhile; ?>

		<?php else :?>
		<?php endif;?>

    </div>
  </div>

</section>
