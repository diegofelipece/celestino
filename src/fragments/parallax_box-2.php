<section class="parallax-box" data-parallax="scroll" data-image-src="<?php the_sub_field('parallax_box-2_bg'); ?>">
	<div class="grid-container full centered-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-y">
			<div class="cell centered-container-inner text-center is-uppercase">
				<?php the_sub_field('parallax_box-2_main-text'); ?>
			</div>
		</div>
	</div>
</section>
