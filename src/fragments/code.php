<?php
//code_row
?>

<section class="section-padding code">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x grid-padding-x">

			<?php if( have_rows('code_row') ):?>
				<?php while ( have_rows('code_row')) : the_row(); ?>
					<div class="cell medium-4">
						<div class="grid-x">
							<?php if (get_sub_field('code_title')) : ?>
								<div class="cell __title is-uppercase text-center">
									<h2><?php the_sub_field('code_title'); ?></h2>
								</div>
							<?php endif; ?>

							<div class="cell __text">
								<?php the_sub_field('code_html'); ?>
							</div>
							<div class="cell text-center">
								<small>Copie o html abaixo e cole na área desejada do seu site.</small>
							</div>
							<div class="cell __code">
								<?php the_sub_field('code_block'); ?>
							</div>

						</div>

					</div>
				<?php endwhile; ?>
			<?php else :?>
			<?php endif;?>

		</div>
	</div>
</section>
