<section class="section-padding text-center magellan-head" data-magellan data-offset="110">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

			<div class="cell __title is-uppercase">
				<h2><?php the_sub_field('magellan_head_title') ?></h2>
			</div>

			<?php if( have_rows('magellan_head_item') ):?>
				<div class="cell __itens">
					<ul class="menu align-center">
					<?php while ( have_rows('magellan_head_item')) : the_row(); ?>

						<li>
							<a href="<?php the_sub_field('magellan_head_item_url') ?>">
								<?php the_sub_field('magellan_head_item_text') ?><br>
								<span class="button round"><i class="icon-down-open-big"></i></span>
							</a>
						</li>

					<?php endwhile; ?>
					</ul>
				</div>
			<?php else :?>
			<?php endif;?>

		</div>
	</div>
</section>
