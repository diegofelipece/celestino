<?php $id = get_sub_field('partners_id'); ?>
<section class="section-padding text-center partners" id="<?= ($id) ? $id : '' ?>">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x grid-margin-y grid-margin-x align-center">

      <div class="cell">
        <div class="button disabled"><i class="icon-down-open-big"></i></div>
      </div>

			<?php $title = get_sub_field('partners_title'); ?>
			<?php if ($title): ?>
				<div class="cell __title">
					<h2><?= $title ?></h2>
				</div>
			<?php endif; ?>

			<div class="cell __map" id="map" data-map='<?=get_partners_json()?>'></div>

			<?php
				$representantes = new WP_Query(array( 'post_type' => 'representante' ));
				while ($representantes->have_posts()) : $representantes->the_post();
			?>

				<div class="cell medium-4 partners_card">
					<div class="grid-x">

						<div class="cell text-center __title">
							<?php the_title();?>
						</div>
						<div class="cell">
							<?php the_field('representante_address'); ?><br>
							<?php the_field('representante_city')?>-<?php the_field('representante_state') ?><br>
							CEP: <?php the_field('representante_cep'); ?><br>

							<?php
								if( have_rows('representante_telefones') ):
						      while ( have_rows('representante_telefones') ) : the_row();
						        $phone = get_sub_field('representante_telefones_each');
						        $phone_filtered = preg_filter('/\)*\(*\s*/', '', $phone);
						        echo '<a href="tel:+55'.$phone_filtered.'" target="_blank">'.$phone.'</a><br>';
						      endwhile;
						    endif;

						    if( have_rows('representante_emails') ):
						      while ( have_rows('representante_emails') ) : the_row();
						        $email = get_sub_field('representante_emails_each');
						        echo '<a href="mailto:'.$email.'" target="_blank">'.$email.'</a><br>';
						      endwhile;
						    endif;

						    $site = get_field('representante_site');
						    if( $site ):
						      echo '<a href="'.$site.'" target="_blank">'.$site.'</a><br>';
						    endif;
							?>
						</div>

					</div>
				</div>

			<?php endwhile; ?>

		</div>
	</div>
</section>
