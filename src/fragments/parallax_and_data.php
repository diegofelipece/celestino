<section class="parallax-and-data has-border-top" data-parallax="scroll" data-image-src="<?php the_sub_field('parallax_and_data_image')?>">

		<div class="grid-container centered-container wow animate fadeIn" data-wow-duration="1s">
			<div class="grid-x">
				<div class="cell medium-6 large-4 centered-container-inner has-place">
					<div class="grid-x grid-margin-x">
						<div class="cell is-uppercase"><?php the_sub_field('parallax_and_data_title') ?></div>
						<div class="cell">
							<?php the_sub_field('parallax_and_data_text') ?>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="data-container">
			<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
				<div class="grid-x grid-margin-x align-center">

					<?php if( have_rows('parallax_and_data_item') ):?>
						<?php while ( have_rows('parallax_and_data_item')) : the_row(); ?>
							<div class="cell medium-shrink data-container-each text-center">
								<div class="__title"><?php the_sub_field('parallax_and_data_item_title'); ?></div>
								<span class="__data"><?php the_sub_field('parallax_and_data_item_text_1'); ?></span><br>
								<span class="__data"><?php the_sub_field('parallax_and_data_item_text_2'); ?></span>
							</div>
						<?php endwhile; ?>
					<?php else :?>
					<?php endif;?>

				</div>
			</div>
		</div>

</section>
