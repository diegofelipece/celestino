<section class="section-padding text-center one-two-three has-border-top" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

			<div class="cell __title is-uppercase">
				<h2><?php the_sub_field('one_two_three_title') ?></h2>
			</div>

			<div class="cell large-8 large-offset-2 __itens">
				<div class="grid-x grid-margin-x grid-margin-y">

					<?php if( have_rows('one_two_three_item') ):?>
						<?php while ( have_rows('one_two_three_item')) : the_row(); ?>
							<div class="cell medium-4 __itens-each">
								<div class="grid-x">
									<div class="cell __itens-each-number"><?php the_sub_field('one_two_three_item_number'); ?></div>
									<div class="cell __itens-each-title"><?php the_sub_field('one_two_three_item_title'); ?></div>
									<div class="cell __itens-each-text"><?php the_sub_field('one_two_three_item_text'); ?></div>
								</div>
							</div>
						<?php endwhile; ?>
					<?php else :?>
					<?php endif;?>

				</div>
			</div>

		</div>
	</div>
</section>
