<section class="contact-box section-padding">

		<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
			<div class="grid-x">
				<div class="cell __title">
					<h3><?php the_sub_field('contact-box') ?></h3>
        </div>
        <div class="cell">
					<?php
	        wp_nav_menu( array(
	          'theme_location' => 'contact',
	          'menu_class' => 'menu',
	          'container' => ''
	        ));
	        ?>
        </div>
			</div>
		</div>

</section>
