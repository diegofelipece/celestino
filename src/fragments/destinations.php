<?php
//wp_reset_postdata();
$destinations = get_sub_field('destinations_list');
?>

<section class="section-padding text-center destinations" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

			<div class="cell __title is-uppercase">
				<h2><?php the_sub_field('destinations_title') ?></h2>
			</div>

      <div class="cell __content">
        <div class="owl-carousel destinations-wrap grid-x"
  				data-owlCarousel='{
            "navText": "",
            "loop": true,
            "responsive" : {
                "0" : {
                  "items": 1,
                  "dots": true,
                  "nav": false,
                  "margin": 10
                },
                "768" : {
                  "items": 2,
                  "dots": false,
                  "nav": true,
                  "margin": 10
                },
                "1024" : {
                  "items": 3,
                  "dots": false,
                  "nav": true,
                  "margin": 10
                }
            }
  				}'>

					<?php foreach ($destinations as $destination ) : ?>
					<?php
						setup_postdata($destination);
						$destination_thumb = get_post_thumbnail_id( $destination->ID );
						// var_dump($destination)
					?>
            <div class="cell auto">

              <div class="card">
                <div class="card-image" data-interchange="
									[<?php getImage($destination_thumb, 'card') ?>, small],
									[<?php getImage($destination_thumb, 'card@2x') ?>, retina],
								">
                	<a href="<?php the_permalink($destination->ID); ?>"></a>
                </div>
                <div class="card-section">
									<a href="<?php the_permalink($destination->ID); ?>">
	                  <h4 class="__title is-uppercase"><?=$destination->post_title?></h4>
	                  <p class="__text"><?=$destination->post_content?></p>
									</a>
									<p>Tipos de visto:</p>
									<div class="button-group align-center">
										<?php if (get_field('destination_tourist', $destination->ID)): ?>
											<a href="<?php the_permalink($destination->ID); ?>#turismo" class="button">Turismo</a>
										<?php endif; ?>
										<?php if (get_field('destination_business', $destination->ID)): ?>
									  	<a href="<?php the_permalink($destination->ID); ?>#negocios" class="button">Negócios</a>
										<?php endif; ?>
										<?php if (get_field('destination_study', $destination->ID)): ?>
									  	<a href="<?php the_permalink($destination->ID); ?>#estudos" class="button">Estudos</a>
										<?php endif; ?>
									</div>
                </div>

              </div>
            </div>
          <?php endforeach; ?>
					<?php wp_reset_postdata(); ?>
        </div>
      </div>

		</div>
	</div>
</section>
