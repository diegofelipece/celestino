<?php
$has_button = get_sub_field('round_itens_item_button');
?>

<section class="section-padding text-center round-itens" data-magellan data-offset="110">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

			<?php if (get_sub_field('round_itens_title')) : ?>
				<div class="cell __title">
					<h2><?php the_sub_field('round_itens_title');?></h2>
				</div>
			<?php endif; ?>

			<div class="cell large-10 large-offset-1 __itens">
				<div class="grid-x grid-margin-x grid-margin-y">

					<?php if( have_rows('round_itens_item') ):?>
						<?php
							while ( have_rows('round_itens_item')) : the_row();
							$url = get_sub_field('round_itens_item_image_url');
						?>
						<div class="cell small-6 medium-auto __itens-each">
							<div class="__itens-each-image" data-interchange="[<?php the_sub_field('round_itens_item_image') ?>, small]">
								<?php if ($url) : ?>
									<a href="<?=$url?>"></a>
								<?php endif; ?>
							</div>
							<?= ($url) ? '<a href="'.$url.'">' : ''?>
								<div class="__itens-each-title"><?php the_sub_field('round_itens_item_text') ?></div>
							<?= ($url) ? '</a>' : ''?>

							<?php if ($has_button) : ?>
								<?php if ($url): ?>
									<a href="<?=$url?>" class="button round"><i class="icon-down-open-big"></i></a>
								<?php else: ?>
									<button class="button round"><i class="icon-down-open-big"></i></button>
								<?php endif; ?>
							<?php endif; ?>

						</div>
						<?php endwhile; ?>
					<?php else :?>
					<?php endif;?>

				</div>
			</div>


		</div>
	</div>
</section>
