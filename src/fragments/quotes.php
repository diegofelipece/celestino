<section class="section-padding quotes has-border-top" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">
		 	<div class="medium-10 medium-offset-1 cell">

				<div class="owl-carousel quotes-wrap grid-x"
				data-owlCarousel='{
					"navText": "",
					"loop": true,
					"items": 1,
					"autoheight": true,
					"responsive" : {
							"0" : {
								"dots": true,
								"nav": false
							},
							"480" : {
								"dots": false,
								"nav": true
							}
					}
				}'>

				<?php if( have_rows('quotes_item') ):?>
					<?php while ( have_rows('quotes_item')) : the_row(); ?>
					<div class="cell quotes-each">
						<div class="grid-x grid-padding-x">

							<div class="cell medium-4 text-right">
								<div class="grid-y">
									<div class="cell __image">
										<img src="<?php the_sub_field('quotes_item_image'); ?>" width="110" alt="">
									</div>
									<div class="cell __title">
										<h3><?php the_sub_field('quotes_item_title'); ?></h3>
									</div>
									<?php if (get_sub_field('quotes_item_subtitle')): ?>
										<div class="cell __subtitle">
											<p><?php the_sub_field('quotes_item_subtitle'); ?></p>
										</div>
									<?php endif; ?>
								</div>
							</div>

							<div class="cell medium-8 __text text-justify">
								<?php the_sub_field('quotes_item_text'); ?>
							</div>

						</div>
					</div>

				<?php endwhile; ?>
				<?php else :?>
				<?php endif;?>
			</div>

		 	</div>
		 </div>
	</div>
</section>
