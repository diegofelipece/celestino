<section class="section-padding text-center squared-list" data-magellan data-offset="110">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x grid-margin-y">

			<div class="cell __title">
				<h2><?php the_sub_field('squared_list_title'); ?></h2>
			</div>

			<div class="cell large-10 large-offset-1 __itens">
				<div class="grid-x grid-margin-x grid-margin-y">

					<?php if( have_rows('squared_list_item') ):?>
						<?php while ( have_rows('squared_list_item')) : the_row(); ?>
							<div class="cell medium-4 __itens-each">
								<div class="grid-x">
									<div class="cell is-uppercase __itens-each-title"><?php the_sub_field('squared_list_item_title') ?></div>
									<div class="cell __itens-each-text">
										<?php the_sub_field('squared_list_item_text') ?>
									</div>
									<a href="<?php the_sub_field('squared_list_item_url') ?>" class="button round"><i class="icon-down-open-big"></i></a>
								</div>
							</div>
						<?php endwhile; ?>
					<?php else :?>
					<?php endif;?>

				</div>
			</div>

		</div>
	</div>
</section>
