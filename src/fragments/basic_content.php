<?php
$has_border_top = get_sub_field('basic_content_border_top');
$id = get_sub_field('basic_content_id');
?>

<section class="section-padding basic-content <?=($has_border_top) ? 'has-border-top' : '';?>" <?= ($id) ? 'id="'.$id.'" data-magellan-target="'.$id.'"' : '' ?>>
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

			<?php if (get_sub_field('basic_content_title')) : ?>
				<div class="cell medium-offset-1 medium-10 __title is-uppercase">
					<h2><?php the_sub_field('basic_content_title'); ?></h2>
				</div>
			<?php endif; ?>

			<div class="cell medium-offset-1 medium-10 __text">
				<?php the_sub_field('basic_content_text'); ?>
			</div>

		</div>
	</div>
</section>
