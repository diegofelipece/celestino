<?php $url = get_sub_field('parallax_mais_url'); ?>
<section class="parallax-mais-info has-border-top" data-parallax="scroll" data-image-src="<?php the_sub_field('parallax_mais_info_bg');?>">

		<div class="grid-container centered-container wow animate fadeIn" data-wow-duration="1s">
			<div class="grid-y">
				<div class="cell medium-6 medium-offset-6 large-5 large-offset-7 centered-container-inner has-airplane text-right is-uppercase">
					<?= ($url) ? '<a href="'.$url.'" class="__clicktag"></a>' : ''?>
					<?php the_sub_field('parallax_mais_info_title');?>
				</div>
			</div>
		</div>

</section>
