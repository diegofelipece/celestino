<section class="section-padding text-center posts-item" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">

      <div class="cell __content">
        <div class="posts-item-wrap grid-x grid-margin-x grid-margin-y align-center">

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="posts-item-each cell medium-4">

              <div class="card">
								<?php
									$thumb_id = get_post_thumbnail_id( $post->ID );
								?>
								<?php if ($thumb_id) : ?>
									<div class="card-image" data-interchange="
										[<?php getImage($thumb_id, 'card') ?>, small],
										[<?php getImage($thumb_id, 'card@2x') ?>, retina],
									">
										<a href="<?php the_permalink()?>"></a>
									</div>
								<?php endif; ?>
                <div class="card-section">
									<a href="<?php the_permalink()?>">
	                  <h4 class="__title is-uppercase"><?php the_title(); ?></h4>
									</a>
									<ul class="menu align-center">
										<?php
										$terms = get_the_terms( $post->ID, 'category' );
										//var_dump($terms);
										foreach ($terms as $term): ?>
											<li><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
										<?php endforeach; ?>
									</ul>
									<div class="__text">
										<a href="<?php the_permalink()?>">
											<?php the_excerpt(); ?>
										</a>
									</div>
									<a href="<?php the_permalink()?>" class="button">Leia mais</a>
                </div>

              </div>
            </div>
					<?php endwhile?>
					<?php else: ?>
					<?php endif; ?>

        </div>
      </div>



		</div>
	</div>
</section>




<?php /*
		<section class="posts-item section-padding">
			<div class="grid-container">
				<div class="grid-x">

						<div class="cell medium-10 medium-offset-1 text-center">
							<h2><?php the_title(); ?></h2>
							<ul class="menu align-center">
								<?php
								$terms = get_the_terms( $post->ID, 'category' );
								//var_dump($terms);
								foreach ($terms as $term): ?>
									<li><a href="<?= get_term_link($term) ?>"><?= $term->name ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>

					<div class="cell medium-8 medium-offset-2">
						<?php the_excerpt(); ?>
					</div>

					<div class="cell medium-8 medium-offset-2">
						<a href="<?php the_permalink()?>" class="button">Ler mais</a>
					</div>

				</div>
			</div>
		</section>
*/ ?>
