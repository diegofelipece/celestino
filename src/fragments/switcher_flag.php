<section class="switcher-flag has-border-top">
  <div class="grid-container wow animate fadeIn" data-wow-duration="1s">
    <div class="grid-x grid-margin-y">

      <?php if( have_rows('switcher_flag_item') ):?>
        <div class="cell text-right">
          <ul class="menu align-right">
            <?php while ( have_rows('switcher_flag_item')) : the_row(); ?>
              <li>
                <a href="<?php the_sub_field('switcher_flag_item_url')?>">
                  <img src="<?php the_sub_field('switcher_flag_item_img')?>">
                  <p><?php the_sub_field('switcher_flag_item_title')?></p>
                </a>
              </li>
            <?php endwhile; ?>
          </ul>
        </div>
      <?php else :?>
      <?php endif;?>

    </div>
  </div>
</section>
