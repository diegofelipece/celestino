
<input type="hidden" name="destinations_json" id="destinations_json" value='<?=get_destinations_JSON();?>'>

<section class="search-box" id="">
	<div class="grid-container wow animate fadeIn" data-wow-duration="1s">
		<div class="grid-x">
			<form role="search" method="get" class="cell">
		    <ul class="menu align-center">
					<li>
						<?php $search_box_text = get_sub_field('search_box_text') ?>
						<p class="__label"><?= ($search_box_text) ?  $search_box_text : 'Busque aqui, outros destinos';?></p>
					</li>
					<li class="autocomplete">
						<?php $search_box_placeholder = get_sub_field('search_box_placeholder') ?>
						<input type="text" class="__field" value="" id="searchform" placeholder="<?= ($search_box_placeholder) ? $search_box_placeholder : 'Digite Aqui ...'; ?>" disabled="disabled" autocomplete="off">
					</li>
	    	</ul>
			</form>
		</div>
	</div>
</section>
