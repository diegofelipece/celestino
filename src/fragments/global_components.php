<?php

// check if the flexible content field has rows of data
if( have_rows('components') ):

     // loop through the rows of data
    while ( have_rows('components') ) : the_row();

        if( get_row_layout() == 'carousel' ):
					get_template_part('fragments/carousel');
        elseif( get_row_layout() == 'parallax_box-1' ):
					get_template_part('fragments/parallax_box-1');
        elseif( get_row_layout() == 'parallax_box-2' ):
					get_template_part('fragments/parallax_box-2');
        elseif( get_row_layout() == 'quotes' ):
					get_template_part('fragments/quotes');
        elseif( get_row_layout() == 'round_itens' ):
					get_template_part('fragments/round_itens');
        elseif( get_row_layout() == 'round_list' ):
					get_template_part('fragments/round_list');
        elseif( get_row_layout() == 'search_box' ):
					get_template_part('fragments/search_box');
        elseif( get_row_layout() == 'parallax_and_data' ):
					get_template_part('fragments/parallax_and_data');
        elseif( get_row_layout() == 'contact_box' ):
					get_template_part('fragments/contact_box');
        elseif( get_row_layout() == 'topics' ):
					get_template_part('fragments/topics');
        elseif( get_row_layout() == 'one_two_three' ):
					get_template_part('fragments/one_two_three');
        elseif( get_row_layout() == 'basic_content' ):
					get_template_part('fragments/basic_content');
        elseif( get_row_layout() == 'parallax_mais_info' ):
					get_template_part('fragments/parallax_mais_info');
        elseif( get_row_layout() == 'squared_list' ):
					get_template_part('fragments/squared_list');
        elseif( get_row_layout() == 'partners' ):
					get_template_part('fragments/partners');
        elseif( get_row_layout() == 'destinations' ):
					get_template_part('fragments/destinations');
        elseif( get_row_layout() == 'magellan_head' ):
					get_template_part('fragments/magellan_head');
        elseif( get_row_layout() == 'topic_head' ):
					get_template_part('fragments/topic_head');
        elseif( get_row_layout() == 'switcher_flag' ):
					get_template_part('fragments/switcher_flag');
        elseif( get_row_layout() == 'code' ):
					get_template_part('fragments/code');
        endif;

    endwhile;

else :

    // no layouts found

endif;

?>
