<?php

require( dirname( __FILE__ ) . '/wp/wp-config.php' );

$requestedID = $_GET['pais'];
$newURL = home_url();
$oldURLS = array(
  array(
    'ID' => '64',
    'name' => 'Estados Unidos',
    'slug' => 'estados-unidos'
  ),
  array(
    'ID' => '40',
    'name' => 'Canada',
    'slug' => 'canada'
  ),
  array(
    'ID' => '18',
    'name' => 'Austrália',
    'slug' => 'australia'
  ),
  array(
    'ID' => '44',
    'name' => 'China',
    'slug' => 'china-republica-popular'
  ),
  array(
    'ID' => '108',
    'name' => 'Japão',
    'slug' => 'japao'
  ),
  array(
    'ID' => '97',
    'name' => 'India',
    'slug' => 'india'
  ),
  array(
    'ID' => '203',
    'name' => 'Vietnã',
    'slug' => 'vietna'
  ),
  array(
    'ID' => '57',
    'name' => 'Egito',
    'slug' => 'egito'
  ),
  array(
    'ID' => '52',
    'name' => 'Cuba',
    'slug' => 'cuba'
  ),
  array(
    'ID' => '162',
    'name' => 'Quênia',
    'slug' => 'quenia'
  ),
  array(
    'ID' => '136',
    'name' => 'Moçambique',
    'slug' => 'mocambique'
  ),
  array(
    'ID' => '100',
    'name' => 'Irã',
    'slug' => 'ira'
  ),
  array(
    'ID' => '109',
    'name' => 'Jordânia',
    'slug' => 'jordania'
  ),
  array(
    'ID' => '180',
    'name' => 'Sri Lanka',
    'slug' => 'sri-lanka'
  ),
  array(
    'ID' => '190',
    'name' => 'Taiwan',
    'slug' => 'taiwan'
  ),
  array(
    'ID' => '140',
    'name' => 'Mianmar',
    'slug' => 'mianmar'
  ),
  array(
    'ID' => '66',
    'name' => 'Etiópia',
    'slug' => 'etiopia'
  ),
  array(
    'ID' => '191',
    'name' => 'TanzÂnia',
    'slug' => 'tanzania'
  ),
  array(
    'ID' => '39',
    'name' => 'Camboja',
    'slug' => 'camboja'
  ),
  array(
    'ID' => '74',
    'name' => 'Gana',
    'slug' => 'gana'
  ),
  array(
    'ID' => '145',
    'name' => 'Nigéria',
    'slug' => 'nigeria'
  )
);

if (isset($requestedID)) {
  //  echo 'Está settado - ver se existe na lista.<br>';

  foreach ($oldURLS as $url) {
    if ($url['ID'] == $requestedID) {
      //echo "Redirect para {$newURL}/destino/{$url['slug']}";
      header("Location: {$newURL}/destino/{$url['slug']}");
      die();
    }
  }
  // echo "Redirect para {$newURL}/destinos";
  header("Location: {$newURL}/destinos");
  die();

} else{
  // echo "Não está settado. Redirect para {$newURL}/destinos";
  header("Location: {$newURL}/destinos");
  die();
};

die();
