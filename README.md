# Celestino Despachante #

Instalation:

``` bash
// Start by cloning the project
git clone https://github.com/diegofelipece/celestino.git

```

Now get WordPress files:

``` bash

// This url will always deliver to you the latest version of WP
wget https://br.wordpress.org/latest-pt_BR.zip

// after that, simply unzip, rename de folder, and remove the .zip
unzip latest-pt_BR.zip && mv wordpress/ wp && rm -r latest-pt_BR.zip

```


``` php
  <?php
  /**
   * Front to the WordPress application. This file doesn't do anything, but loads
   * wp-blog-header.php which does and tells WordPress to load the theme.
   *
   * @package WordPress
   */

  /**
   * Tells WordPress to load the WordPress theme and output it.
   *
   * @var bool
   */
  define('WP_USE_THEMES', true);

  /** Loads the WordPress Environment and Template */
  require( dirname( __FILE__ ) . '/wp/wp-blog-header.php' );
```

We have **wp-config.php** with some configs mores, if you wanted to use it, use **wp-config-tangerine.php** as a sample, rename it as **wp-config.php** and move it for **/wp** folder.

You need to add the following constant on wp-config.php

``` php

$GLOBALS['WP_DEVMODE'] = true;

```

Now you'll need to install the components, using **Node** and **Bower**

``` bash
npm install
bower install
```

As long as you have adjusted you **wp-config.php**, run **Gulp** to test

``` bash
gulp
```

Now if everything works well, you can start working!

You will be working in **src/** folder, and your theme is inside **wp-content**, but don't worry, Gulp it's taking care of it.

____

Created by [Diego Esteves](http://diegoesteves.ink) under The MIT License (MIT)
